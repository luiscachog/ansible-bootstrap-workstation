#!/bin/bash
#===============================================================================
#
#          FILE:  integration-test.sh
#
#         USAGE:  ./integration-test.sh
#
#   DESCRIPTION:  Script to run all the molecule unitary tests together
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Luis Cacho, luiscachog@gmail.com
#       VERSION:  0.1
#       CREATED:  2018/11/11
#     CHANGELOG:  Initial version of the script
#===============================================================================

#===============================================================================
#				USER VARIABLES
#===============================================================================
SCRIPTVER="0.1"
# Set to 1 to enable debugging, printing of variables.
# Can be overridden in config file.
DEBUG=0

set -e
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename "${__file}")"


cd $__dir/roles/config-packages
molecule test
