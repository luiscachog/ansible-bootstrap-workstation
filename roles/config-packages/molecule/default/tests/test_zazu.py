from testinfra.utils.ansible_runner import AnsibleRunner
import pytest
import os
import logging
import testinfra.utils.ansible_runner
import collections

logging.basicConfig(level=logging.DEBUG)
# DEFAULT_HOST = 'all'
VAR_FILE = "../../vars/zazu-snippets.yml"

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
# runner.get_hosts(DEFAULT_HOST)


@pytest.fixture
def ansible_variables(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        VAR_FILE
    )
    return variables['ansible_facts']


def converttostr(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(converttostr, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(converttostr, data))
    else:
        return data


def test_zazu_prefabs(host, ansible_variables):
    dict_variables = converttostr(ansible_variables)
    myprefabs = dict_variables['__zazu_snippets']
    path = "/home/luis7238/.zazu/plugins/tinytacoteam/zazu-snippets/snippets/"
    for prefab in myprefabs:
        tempfile = path + prefab
        assert host.file(tempfile).exists
        assert host.file(tempfile).is_file
        assert host.file(tempfile).user == 'luis7238'
        assert host.file(tempfile).group == 'luis7238'
        assert "Luis Cacho" in host.file(tempfile).content_string
