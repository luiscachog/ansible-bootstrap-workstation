from __future__ import absolute_import
from __future__ import unicode_literals
from testinfra.utils.ansible_runner import AnsibleRunner
import os
import pytest
import logging
import testinfra.utils.ansible_runner
import collections


logging.basicConfig(level=logging.DEBUG)
# DEFAULT_HOST = 'all'
VAR_FILE = "../../vars/packages-npm.yml"

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
# runner.get_hosts(DEFAULT_HOST)


@pytest.fixture
def ansible_variables(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        VAR_FILE
    )
    return variables['ansible_facts']


def converttostr(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(converttostr, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(converttostr, data))
    else:
        return data


@pytest.mark.parametrize('npm_commands', [
  'coinmon --version',
  'parse-columns --version',
  'bower --version',
  'pokemon',
  # 'namecheap'
])
def test_install_npm_pkgs(host, npm_commands):
    command = host.run_test(npm_commands)
    assert command.rc == 0
