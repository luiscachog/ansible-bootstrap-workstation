from __future__ import absolute_import
from __future__ import unicode_literals
from testinfra.utils.ansible_runner import AnsibleRunner
import os
import pytest
import logging
import testinfra.utils.ansible_runner
import collections


logging.basicConfig(level=logging.DEBUG)
# DEFAULT_HOST = 'all'
VAR_FILE = "../../vars/packages-apt.yml"

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
# runner.get_hosts(DEFAULT_HOST)


@pytest.fixture
def ansible_variables(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        VAR_FILE
    )
    return variables['ansible_facts']


def converttostr(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(converttostr, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(converttostr, data))
    else:
        return data


def test_install_common_pkgs(host, ansible_variables):
    dict_packages = converttostr(ansible_variables)
    for i in dict_packages['__common_pkgs']:
            package = host.package(i)
            assert package.is_installed


def test_install_dev_pkgs(host, ansible_variables):
    dict_packages = converttostr(ansible_variables)
    for i in dict_packages['__dev_pkgs']:
            package = host.package(i)
            assert package.is_installed


def test_install_tools_pkgs(host, ansible_variables):
    dict_packages = converttostr(ansible_variables)
    for i in dict_packages['__tools_pkgs']:
            package = host.package(i)
            assert package.is_installed


def test_install_infra_pkgs(host, ansible_variables):
    dict_packages = converttostr(ansible_variables)
    for i in dict_packages['__infra_pkgs']:
            package = host.package(i)
            assert package.is_installed


def test_install_themes_pkgs(host, ansible_variables):
    dict_packages = converttostr(ansible_variables)
    for i in dict_packages['__themes_pkgs']:
            package = host.package(i)
            assert package.is_installed


def test_install_rs_pkgs(host, ansible_variables):
    dict_packages = converttostr(ansible_variables)
    for i in dict_packages['__rs_pkgs']:
            package = host.package(i)
            assert package.is_installed
