from testinfra.utils.ansible_runner import AnsibleRunner
import pytest
import os
import logging
import testinfra.utils.ansible_runner
import collections

logging.basicConfig(level=logging.DEBUG)
# DEFAULT_HOST = 'all'
VAR_FILE = "../../defaults/main.yml"

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
# runner.get_hosts(DEFAULT_HOST)


@pytest.fixture
def ansible_variables(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        VAR_FILE
    )
    return variables['ansible_facts']


def converttostr(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(converttostr, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(converttostr, data))
    else:
        return data


def test_group(host, ansible_variables):
    dict_variables = converttostr(ansible_variables)
    mygroup = dict_variables['__group']
    mygid = dict_variables['__gid']
    assert host.group(mygroup).exists
    assert host.group(mygroup).gid == mygid


def test_user(host, ansible_variables):
    dict_variables = converttostr(ansible_variables)
    myuser = dict_variables['__user']
    myuid = dict_variables['__uid']
    mygid = dict_variables['__gid']
    myshell = dict_variables['__shell']
    mypassword = dict_variables['__user_password']

    assert host.user(myuser).exists
    assert host.user(myuser).uid == myuid
    assert host.user(myuser).gid == mygid
    assert host.user(myuser).shell == myshell
    assert host.user(myuser).password == mypassword
