from testinfra.utils.ansible_runner import AnsibleRunner
import pytest
import os
import logging
import testinfra.utils.ansible_runner
import collections

logging.basicConfig(level=logging.DEBUG)
# DEFAULT_HOST = 'all'
VAR_FILE = "../../vars/directories.yml"
DEFAULT_FILE = "../../defaults/main.yml"

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
# runner.get_hosts(DEFAULT_HOST)


@pytest.fixture
def ansible_variables(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        VAR_FILE
    )
    return variables['ansible_facts']


@pytest.fixture
def ansible_defaults(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        DEFAULT_FILE
    )
    return variables['ansible_facts']


def converttostr(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(converttostr, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(converttostr, data))
    else:
        return data


# TODO = Replace variables from roles
@pytest.mark.parametrize('config_file', [
  '/home/luis7238/.npmrc',
])
def test_initialize(host, config_file):
    mycontent = host.file(config_file).content_string
    assert 'prefix=/home/luis7238/.node_global_modules' in mycontent
