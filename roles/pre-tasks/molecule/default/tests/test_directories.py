from testinfra.utils.ansible_runner import AnsibleRunner
import pytest
import os
import logging
import testinfra.utils.ansible_runner
import collections

logging.basicConfig(level=logging.DEBUG)
# DEFAULT_HOST = 'all'
VAR_FILE = "../../vars/directories.yml"
DEFAULT_FILE = "../../defaults/main.yml"

TESTINFRA_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
# runner.get_hosts(DEFAULT_HOST)


@pytest.fixture
def ansible_variables(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        VAR_FILE
    )
    return variables['ansible_facts']


@pytest.fixture
def ansible_defaults(host):
    variables = runner.run(
        TESTINFRA_HOSTS,
        'include_vars',
        DEFAULT_FILE
    )
    return variables['ansible_facts']


def converttostr(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(converttostr, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(converttostr, data))
    else:
        return data

# TODO = Replace variables from roles
# def test_temporal_directories(host, ansible_variables):
#     dict_variables = converttostr(ansible_variables)
#     mytmp = dict_variables['__tmp_dir']
#     mydirs = dict_variables['__temporal_directories']
#     print(mytmp)
#     print(mydirs)
#     for tmpdir in mydirs:
#         directory = mytmp + '/' + tmpdir
#         print(directory)
#         assert host.file(directory)


# def test_work_directories(host, ansible_variables, ansible_defaults):
#     dict_variables = converttostr(ansible_variables)
#     dict_defaults =  converttostr(ansible_defaults)
#     myuser = ansible_defaults['__user']
#     mydirs = dict_variables['__temporal_directories']
#     for tmpdir in mydirs:
#         directory = mytmp + '/' + tmpdir
#         assert host.file(directory)


@pytest.mark.parametrize('directories', [
  '/home/luis7238/.node_global_modules',
  '/tmp/.ansible_tmp/fonts',
  '/tmp/.ansible_tmp/icons',
  '/tmp/.ansible_tmp/debpackages',
  '/tmp/.ansible_tmp/uncompressed',
  '/home/luis7238/.zazu/plugins/tinytacoteam/zazu-snippets/snippets"'
])
def test_work_directories(host, directories):
    mydir = host.file(directories)
    assert mydir.exists
    assert mydir.is_directory
