# ansible-bootstrap-desktop

Ansible playbooks to automate the bootstrap process on my personal system.
Sets up and configures all of the software I use.

## What it does 

1. Create a username 
1. Install Packages based on APT repositories (You can edit the list on roles/install-packages/vars/packages-apt.yml)
2. Install Packages based on PIP (You can edit the list on roles/install-packages/vars/packages-pip.yml)
2. Install Packages based on NPM (You can edit the list on roles/install-packages/vars/packages-npm.yml)
2. Install Packages based on .deb, .zip, .tar, .tar.gz (You can edit the list on roles/install-packages/vars/packages-extras.yml)
